/* **** set vars and words on content divs **** */
// define dataLayer
window.dataLayer = window.dataLayer || [];
// number of words in content - https://stackoverflow.com/questions/765419/javascript-word-count-for-any-given-dom-element
function get_text(el) {
    ret = "";
    var length = el.childNodes.length;
    for(var i = 0; i < length; i++) {
        var node = el.childNodes[i];
        // If the node is not a comment (nodeType 8)
        if(node.nodeType != 8) {
            // think this is saying if nodeType is not an element then get text value else get the child node and reloop
            ret += node.nodeType != 1 ? node.nodeValue : get_text(node);
        }
    }
    return ret;
}
// gets text of div
var words = get_text(document.getElementById("wordCount-js"));
// gets word count
var count = words.split(' ').length;
console.log("word count: "+ count);
// Slow reader: 150-200 wpm. Average adult reader: 250wpm. College student: 300wpm
var wordsPerMinute = 250;
// read time in milliseconds
var readTime = count / wordsPerMinute * 60000;
console.log("read time: "+ readTime);
// updated read time cut in 1/2 for updated engagement metrics
var updatedReadTime = readTime / 2;
console.log("updated read time:" + updatedReadTime);
// timer and visitor
var startTime = new Date().getTime();
console.log("Time: "+ startTime);

/* **** detect if user scrolls 1/2 way down the article **** */
var bonJovi = false;
function scroll_detection(){
    // article content div
    var wysiwygTarget = document.getElementById("wordCount-js");
    // get article body height, and how far it is from the top of scrren then check if scroll past 1/2 way through it
    if(window.scrollY > (wysiwygTarget.offsetTop + (wysiwygTarget.offsetHeight / 2))){
        bonJovi = true;
        console.log("Whoa, we're half way there, whoa-oh, livin on a prayer!");
        // updated engagement types
        dataLayer.push({'event': 'engagement-scrolldepth-50'});
        // remove scroll detection once reach 1/2 point
        window.removeEventListener("scroll", scroll_detection);
    }
}
window.addEventListener("scroll", scroll_detection);

/* **** detect if user has been on the page for the updated read time **** */
// set timer
let timer = setInterval(countDown, 1000);
function countDown(){
    var currentTime = new Date().getTime();
    var elapsedTime = currentTime - startTime;
    if(elapsedTime > updatedReadTime){
        // if readtime true
        dataLayer.push({'event': 'engagement-readtime-50'});
        console.log("Take my hand, we've read it, I swear");
        window.clearInterval(timer);
    }
}

/* **** detect if user scrolled to the end of the content **** */
var scroll = false;
window.addEventListener("scroll", function() {
    // div at bottom of article
    var elementTarget = document.getElementById("scroll-js");
    // get window height
    var h = window.innerHeight;
    if (window.scrollY > (elementTarget.offsetTop + elementTarget.offsetHeight - h)) {
        scroll = true;
        console.log("You've scrolled to the end");
    }
});

/* **** old engagement metrics **** */
function calcTime(){
    var currentTime = new Date().getTime();
    var elapsedTime = currentTime - startTime;
    var timeToRead = count / wordsPerMinute;

    // visior types
    if (elapsedTime < readTime && scroll == false) {
        var audience = "Bouncer";
        gtag("event", audience, {
            "event_category": "content-engagement",
            "event_label": timeToRead,
            "non_interaction": true,
            "metric8": "1"
        });
    }
    else if (elapsedTime < readTime && scroll == true) {
        var audience = "Skimmer";
        gtag("event", audience, {
            "event_category": "content-engagement",
            "event_label": timeToRead,
            "non_interaction": true,
            "metric7": "1"
        });
    }
    else if (elapsedTime > readTime && scroll == false) {
        var audience = "Dweller";
        gtag("event", audience, {
            "event_category": "content-engagement",
            "event_label": timeToRead,
            "non_interaction": true,
            "metric10": "1"
        });
    }
    else if (elapsedTime > readTime && scroll == true) {
        var audience = "Consumer";
        gtag("event", audience, {
            "event_category": "content-engagement",
            "event_label": timeToRead,
            "metric9": "1"
        });
    }
    console.log("audience: "+ audience +" - scroll: "+ scroll +" -  time: "+ elapsedTime);
}

window.addEventListener("beforeunload", calcTime);
