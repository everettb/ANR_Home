// javascript

const charts = document.getElementsByClassName("d3-chart");
var width = 975;
var height = 600;

// function drawVerticalBar() {
//
// }
//
// function drawHorizontalBar() {
//
// }

async function drawGroupedBar() {
  var containerData = document.getElementById("js-d3-chart-" + i);
  var containerSvg = d3.select("#js-d3-chart-" + i);
  var data = Object.assign(await d3.csv(containerData.dataset.graphCsv, d3.autoType), {y: containerData.dataset.yAxisLabel});

  var groupKey = data.columns[0],
      keys = data.columns.slice(1),
      margin = ({top: 10, right: 10, bottom: 20, left: 50});

    var xScale0 = d3.scaleBand()
      .domain(data.map(d => { return d[groupKey]; } ))
      .rangeRound([margin.left, width - margin.right])
      .paddingInner(0.1);

    var xScale1 = d3.scaleBand()
      .domain(keys)
      .rangeRound([0, xScale0.bandwidth()])
      .padding(0.05);

    var yScale = d3.scaleLinear()
      .domain([0, d3.max(data, d => d3.max(keys, key => d[key]))]).nice()
      .rangeRound([height - margin.bottom, margin.top]);

    var xAxis = d => d
      .attr("transform", `translate(0,${height - margin.bottom})`)
      .call(d3.axisBottom(xScale0).tickSizeOuter(0))
      .call(d => d.select(".domain"))
        .attr("font-size", 20);

    var yAxis = d => d
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(yScale).ticks(null, "s"))
        .attr("font-size", 20)
      .call(d => d.select(".domain"))
        .attr("font-size", 20)
      .call(d => d.select(".tick:last-of-type text").clone()
          .attr("x", 3)
          .attr("text-anchor", "start")
          .attr("font-weight", "bold")
          .attr("font-size", 20)
          .text(data.y));

    legend = svg => {
        const g = svg
            .attr("transform", `translate(${width},-75)`)
            .attr("text-anchor", "end")
            .attr("font-family", "sans-serif")
            .attr("font-size", 20)
          .selectAll("g")
          .data(color.domain().slice().reverse())
          .enter()
          .append("g")
            .attr("transform", (d, i) => `translate(0,${i * 20})`);

        g.append("rect")
            .attr("x", -19)
            .attr("width", 19)
            .attr("height", 19)
            .attr("fill", color);

        g.append("text")
            .attr("x", -24)
            .attr("y", 9.5)
            .attr("dy", "0.35em")
            .text(d => d);
    }

    var color = d3.scaleOrdinal()
      .range(["#d1de3f", "#94ae4a", "#0db14b", "#cb5a28", "#f08521", "#c89a58", "#e8d9b5"]);

    var klass = "groupedBar";

    const svg = containerSvg
      .append("svg")
        .attr("class", klass)
        .attr("height", height)
        .attr("width", width)
        .attr("viewBox", [0, 0, width, height])
        .attr("style", "width: 100%;");

    svg.selectAll("g")
      .data(data)
      .enter()
      .append("g")
        .attr("transform", d => `translate(${xScale0( d[groupKey])},0)`)
      .selectAll("rect")
      .data(d => keys.map(key => ({key, value: d[key]})))
      .enter()
      .append("rect")
        .attr("x", d => xScale1(d.key))
        .attr("y", d => yScale(d.value))
        .attr("width", xScale1.bandwidth())
        .attr("height", d => yScale(0) - yScale(d.value))
        .attr("fill", d => color(d.key));

    svg.append("g")
      .call(xAxis);

    svg.append("g")
      .call(yAxis);

    svg.append("g")
      .call(legend);

    // var table = d3.select(containerData)
    //   .append('table');
    //
    //
    // table.append('thead').append('tr')
    //   .selectAll('th')
    //   .data(data.columns).enter()
    //   .append('th')
    //   .text(d => d);
    //
    // table.append('tbody')
    //   .selectAll('tr')
    //   .data(data)
    //   .enter()
    //   .append('tr');
}

for(var i = 0; i < charts.length; i++) {
  if (charts[i].dataset.graphType == "grouped-bar") {
    drawGroupedBar();
  } else if (charts[i].dataset.graphType == "vertical-bar") {
    console.log("vertical bar chart");
  } else if (charts[i].dataset.graphType == "pie") {
    console.log("pie chart");
  }
}
// drawVerticalBar();
// drawHorizontalBar();
