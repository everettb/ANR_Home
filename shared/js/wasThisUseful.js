const likeObj = document.querySelector('#js-like');
const dislikeObj = document.querySelector('#js-dislike');
const feedbackForm = document.querySelector('#js-feedbackFormWrapper');

function likeClick() {
  likeObj.classList.add("js-active");
  likeObj.disabled = true;
  dislikeObj.classList.add("js-not-active");
  dislikeObj.disabled = true;
  gaTrackLikes();
};

function dislikeClick() {
  likeObj.classList.add("js-not-active");
  likeObj.disabled = true;
  dislikeObj.classList.add("js-active");
  dislikeObj.disabled = true;
  feedbackForm.removeAttribute("hidden");
};

function gaTrackLikes() {
  var source = likeObj.getAttribute('data-source');
  gtag('event', 'Like', {
  'event_category': 'content-feedback',
  'event_label': source,
  'metric5': '1'
  });
}

function gaTrackDislikes() {
  var source = document.getElementById("articleFeedback").value;
  gtag('event', 'Dislike', {
  'event_category': 'content-feedback',
  'event_label': source,
  'metric6': '1'
  });

  dataLayer.push({
    'feedback_text':source,
    'event': 'feedback-form-submitted'
  });
  
  document.getElementById("js-feedbackFormWrapper").innerHTML = "<p>Thanks for your feedback!</p>"
}
