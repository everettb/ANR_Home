var audioObj = document.getElementsByTagName("audio");

var i;
for (i = 0; i < audioObj.length; i++) {
  var source = audioObj[i].getElementsByTagName("source")[0].getAttribute("src");
  audioObj[i].addEventListener("play", gaTrackPlay);
  // audioObj[i].addEventListener("ended", gaTrackPlay);
}

function gaTrackPlay() {
  gtag('event', 'Play', {
  'event_category': 'value-multimedia',
  'event_label': source,
  'value': 20
});
}
