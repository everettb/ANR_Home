// On Ready cross browser ====================================================//
(function(funcName, baseObj) {
    // The public function name defaults to window.docReady
    // but you can pass in your own object and own function name and those will be used
    // if you want to put them in a different namespace
    funcName = funcName || "docReady";
    baseObj = baseObj || window;
    var readyList = [];
    var readyFired = false;
    var readyEventHandlersInstalled = false;

    // call this when the document is ready
    // this function protects itself against being called more than once
    function ready() {
        if (!readyFired) {
            // this must be set to true before we start calling callbacks
            readyFired = true;
            for (var i = 0; i < readyList.length; i++) {
                // if a callback here happens to add new ready handlers,
                // the docReady() function will see that it already fired
                // and will schedule the callback to run right after
                // this event loop finishes so all handlers will still execute
                // in order and no new ones will be added to the readyList
                // while we are processing the list
                readyList[i].fn.call(window, readyList[i].ctx);
            }
            // allow any closures held by these functions to free
            readyList = [];
        }
    }

    function readyStateChange() {
        if ( document.readyState === "complete" ) {
            ready();
        }
    }

    // This is the one public interface
    // docReady(fn, context);
    // the context argument is optional - if present, it will be passed
    // as an argument to the callback
    baseObj[funcName] = function(callback, context) {
        if (typeof callback !== "function") {
            throw new TypeError("callback for docReady(fn) must be a function");
        }
        // if ready has already fired, then just schedule the callback
        // to fire asynchronously, but right away
        if (readyFired) {
            setTimeout(function() {callback(context);}, 1);
            return;
        } else {
            // add the function and context to the list
            readyList.push({fn: callback, ctx: context});
        }
        // if document already ready to go, schedule the ready function to run
        if (document.readyState === "complete") {
            setTimeout(ready, 1);
        } else if (!readyEventHandlersInstalled) {
            // otherwise if we don't have event handlers installed, install them
            if (document.addEventListener) {
                // first choice is DOMContentLoaded event
                document.addEventListener("DOMContentLoaded", ready, false);
                // backup is window load event
                window.addEventListener("load", ready, false);
            } else {
                // must be IE
                document.attachEvent("onreadystatechange", readyStateChange);
                window.attachEvent("onload", ready);
            }
            readyEventHandlersInstalled = true;
        }
    }
})("docReady", window);

// makeRequest ===============================================================//
function makeRequest (opts) {
  return new Promise(function (resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open(opts.method, opts.url);
    xhr.onload = function () {
      if (this.status >= 200 && this.status < 300) {
        resolve(xhr.response);
      } else {
        reject({
          status: this.status,
          statusText: xhr.statusText
        });
      }
    };
    xhr.onerror = function () {
      reject({
        status: this.status,
        statusText: xhr.statusText
      });
    };
    if (opts.headers) {
      Object.keys(opts.headers).forEach(function (key) {
        xhr.setRequestHeader(key, opts.headers[key]);
      });
    }
    var params = opts.params;
    // We'll need to stringify if we've been given an object
    // If we have a string, this is skipped.
    if (params && typeof params === 'object') {
      params = Object.keys(params).map(function (key) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
      }).join('&');
    }
    xhr.send(params);
  });
}

// Parse json and build callout ==============================================//
function buildCallout(data) {
  var a = JSON.parse(data);
  var c = a.contentlets[0];

  var insert = '<div class="callout"><figure><a href="'+c.URL_MAP_FOR_CONTENT+'"><img src="/contentAsset/image/'+c.image+'/fileAsset/filter/Resize,Crop/resize_w/200/crop_w/200/crop_h/200/crop_x/0/crop_y/0" alt="'+c.givenName+' '+c.familyName+' '+c.honorificSuffix+'"></a></figure><div class="text"><h3><a href="'+c.URL_MAP_FOR_CONTENT+'">'+c.givenName+' '+c.familyName+' '+c.honorificSuffix+'</a></h3><hr><p>'+c.jobTitle+'</p><p><a href="mailto:'+c.email+'">'+c.email+'</a></p><p>'+c.telephone+'</p><a class="readmore" href="'+c.URL_MAP_FOR_CONTENT+'">read more <span>&rarr;</span></a></div><div class="clear"></div></div>';

  var div = document.getElementById("callout1");
  div.innerHTML = insert;
}

// Check if callout divs contain contentlet identifiers ======================//
function idenCheck(callout) {
  if (callout.hasAttribute("data-dotcms-identifier")) {
    // get iden value
    var iden = callout.getAttribute("data-dotcms-identifier");

    // check if iden is set, not blank/null
    if(iden) {
      return iden;
    } else {
      return false;
    }
  }
}

var devUrl = "dev.msu.dotcmscloud.com";
var liveUrl = "www.canr.msu.edu";

  // set url parameters
  // return json object
  // limit set to 1
  var request;
  var host  = "https://" + devUrl + "/api/content/id/";
  var iden;

// Use DocReady, roll out ====================================================//
docReady(function(){
  var callouts = document.getElementsByClassName("callout");

  // foreach callout found
  if(callouts.length > 0) {
    var step = callouts.length;
    for(var i = 0; i < step; i++){
      // pass callout object to function idenCheck
      var iden = idenCheck(callouts[i]);
      // make request
      if(iden) {
        // Headers and params are optional
        var make = makeRequest({
          method: 'GET',
          url: host + iden
        })
        .then(function (data) {
          buildCallout(data);
        })
        .catch(function (err) {
          console.error('Augh, there was an error!', err.statusText);
        });
      }
    }
  }
});
