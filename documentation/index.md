need preview versions of major widgets and landing pages, as well as per-field documentation and widget options, preferably all in one place. include relationships.

# Widget / Landing Page Documentation
_This documentation provides information on single content entries and their corresponding widgets._
## Content Type / Contentlet
Defines the parent container of the individual contentlets. A contentlet is an individual _content type entry_ (piece of content consisting of various text fields, images, etc...).
## Contentlet on the Website
### (1) Widget
* dotCMS Utilizes widgets to display lists of contentlets on the website. Widgets require certain parameters to be defined. You may also resuse widgets by choosing a previously created widget.
### (2) Detail Page
* This is commonly referred to as the landing page, or the dedicated page that typically displays only immediate Article content and any Article Relationship content. This is defined in the Content Type properties.

## Article

<-- widget preview | detail page preview -->

#### Fields
* Host/folder
** This specifies which site the article belongs to.
* Title
** The title of the article. This will also auto-create the url title.
* URL Title
** Derived from the title field.
* Summary
** A short summary of the article. Displayed in widget listings and on the article detail page below the title.
* Image
** The main article image which will be automatically cropped and resized for widget listings as well as article detail pages.
* Image Description
** The alternative text for screen readers. Accessibility requirement.
* Image Caption
** Optional caption for the main image in the article. Will be displayed on the detail (landing) page.
* Social Media Image
** This image will be automatically used for social media sharing. According to current [[Open_graph#Our_Format_as_of_March_2017|social meta]], should be at least 1200px width x 600px height.
* Date Published
** The date of publish.
* Author Text
** If the author is not a "Person" content entry in the CMS, enter the name here.
* Content
** Add and format the main article text/imagery in this field.
* Relationships
** Dataset - relate this article to a dataset.
** Person - relate this article to a person. This will act as the author of the article.
** Series - relate this article to a series.
** Organization - relate this article to an Organization.
** Person - relate this article to a person who is featured in the article.
** Article - relate this article to articles that are similar in context/content to this article.
** Place - relate a place/location to this article - relevant to this article's context.
** Publication Issue - relate a publication to this article.
** Article - related articles.

<-- detail page breakdown -->

### Widget
Typically multiple entries in a list. Configurable attributes.
#### Fields
* Title
** Used internally by dotCMS to name this particular widget.
* Headline
** The title that will be displayed above the widget content on the website.
* Headline Type
** The size, style of the heading.
* Number of results
** Limit the total number of entries to display.
* Pagination
** Display the total number of results over a set number of pages, depending on the total entries available.
* Items per page
** If pagination is checked, how many items to show per page.

<-- widget breakdown -->

#### Developers
##### Content Type Detail Page Template
* //anr.msu.edu/shared/common_template/article-detail.vtl
##### Widget Template
* //anr.msu.edu/shared/vtl/widgets/common_template/article-listing.vtl

========================================================================================================================

## Event

<-- widget preview | detail page preview -->

#### Fields
* Host/folder
** This specifies which site the event belongs to.
* Title
** The title of the event. This will also auto-create the url title.
* URL Title
** Derived from the title field.
* Summary
** A short summary of the event. Displayed in widget listings and on the event page below the title.
* Image
** The main event image which will be automatically cropped and resized for widget listings as well as event detail pages.
* Image Description
** The alternative text for screen readers. Accessibility requirement.
* Image Caption
** Optional caption for the main image in the event. Will be displayed on the detail (landing) page.
* Social Media Image
** This image will be automatically used for social media sharing. According to current [[Open_graph#Our_Format_as_of_March_2017|social meta]], should be at least 1200px width x 600px height.
* Date Published
** The date of publish.
* Start Date
** The start date and time. If it is an "all day event" check the checkbox.
* Time
** If you wish to specify a specific time via text and not via the Start field, use this field.
* End Date
** The end date and time.
* Repeats
** Does this event repeat? If so, select the applicable timeline and required repeat parameters.
* Location
** If no relatable Place/Location exists in the CMS, fill out this field with location information.
* Contact information
** If no relatable contact person exists in the CMS, fill out this field with contact information for the event - this could be a phone number, email address, person's name, organization, etc.
* Description
** Add and format the main event text/imagery in this field.
* Link
** Use this link for registration information.
* Relationships
** Places - relate a place as a location for this event.
** Organization - relate this event to an organization.
** Dataset - relate this event to a dataset.
** Place - relate this event to a place.
** Person - relate a person as the point of contact for the event.

<-- detail page breakdown -->

### Widget
Typically multiple entries in a list. Configurable attributes.

### Fields
Widget / Detail Page
Label | Type | Variable Name | Required? | Indexed? (searchable) | Show in Content Search list?
What purpose does this field have on the frontend? Backend?

<-- widget breakdown -->

#### Developers
##### Content Type Detail Page Template
* //anr.msu.edu/shared/common_template/event-detail.vtl
##### Widget Template
* //anr.msu.edu/shared/vtl/widgets/common_template/event-listing.vtl

========================================================================================================================

## Person

<-- widget preview | detail page preview -->

### Fields
* Host/folder
  * This specifies which site the person belongs to.
* Title
  * The name of the person (usually first and last name). This will also auto-create the url title.
* URL Title
  * Derived from the title field.
* Image
  * The main image of the person - typically a portrait image - which will be automatically cropped and resized for widget listings as well as person detail pages.
* Social Media Image
  * This image will be automatically used for social media sharing. According to current [[Open_graph#Our_Format_as_of_March_2017|social meta]], should be at least 1200px width x 600px height.
* First Name
* Middle Name
* Last Name
* Honorific Suffix
  * E.g. Ph.D.
* Alternate Name
* Job Title
* Address
  * The professional's address (this person can be contacted at).
* Email
* MSU NetID
* Fax Number
* Telephone
* Cell Phone
* Organization
  * If no relatable Organization (that the person belongs to / is a part of) exists in the CMS, add it here.
* Content
  * Add and format information about the person (text/imagery) in this field.
* Relationships
  * Dataset -
  * Organization - the organization(s) this person works for.
  * Place - the location at which this person works.
  * Series -
  * Event - the events this person is responsible for / is the point of contact.
  * Organization - this person is a member of.
  * Course - the courses this person instructs.
  * Article - featured in these articles.
  * Digital Document - authored by this person.
  * Article - authored by this person.

<-- detail page breakdown -->

### Widget
Typically multiple entries in a list. Configurable attributes.
#### Fields
* Title
** Used internally by dotCMS to name this particular widget.
* Headline
** The title that will be displayed above the widget content on the website.
* Headline Type
** The size, style of the heading.
* Number of results
** Limit the total number of entries to display.
* Pagination
** Display the total number of results over a set number of pages, depending on the total entries available.
* Items per page
** If pagination is checked, how many items to show per page.

<-- widget breakdown -->

#### Developers
##### Content Type Detail Page Template
* //anr.msu.edu/shared/common_template/person-detail.vtl
##### Widget Template
* //anr.msu.edu/shared/vtl/widgets/common_template/person-listing.vtl

==================================================================================================

## Course

### Fields
* Host/folder
** This specifies which site the course belongs to.
* Title
** The title of the course. This will also auto-create the url title.
* URL Title
** Derived from the title field.
* Summary
** A short summary of the course. Displayed in widget listings and on the course page below the title.
* Image
** The main course image which will be automatically cropped and resized for widget listings as well as course detail pages.
* Image Description
** The alternative text for screen readers. Accessibility requirement.
* Social Media Image
** This image will be automatically used for social media sharing. According to current [[Open_graph#Our_Format_as_of_March_2017|social meta]], should be at least 1200px width x 600px height.
* Course Code
* Course Credits
* Content
** Add and format information about the course (text/imagery) in this field.
* Relationships
** Dataset - belongs to this Major.
** Person - the instructor of this course.
** Organization - the course belongs to this organization.
** Place - the location where this course is held.

<-- detail page breakdown -->

### Widget
Typically multiple entries in a list. Configurable attributes.
#### Fields
* Title
** Used internally by dotCMS to name this particular widget.
* Headline
** The title that will be displayed above the widget content on the website.
* Headline Type
** The size, style of the heading.
* Number of results
** Limit the total number of entries to display.
* Pagination
** Display the total number of results over a set number of pages, depending on the total entries available.
* Items per page
** If pagination is checked, how many items to show per page.

<-- widget breakdown -->

#### Developers
##### Content Type Detail Page Template
* //anr.msu.edu/shared/common_template/article-detail.vtl
##### Widget Template
* //anr.msu.edu/shared/vtl/widgets/common_template/article-listing.vtl

==================================================================================================

## Digital Document

### Fields
* Host/folder
** This specifies which site the document belongs to.
* Title
** The title of the document. This will also auto-create the url title.
* URL Title
** Derived from the title field.
* Summary
** A short summary of the document. Displayed in widget listings and on the document page below the title.
* Image
** The main document image which will be automatically cropped and resized for widget listings as well as document detail pages.
* Image Description
** The alternative text for screen readers. Accessibility requirement.
* Social Media Image
** This image will be automatically used for social media sharing. According to current [[Open_graph#Our_Format_as_of_March_2017|social meta]], should be at least 1200px width x 600px height.
* File
** Select which file will represent the downloadable file.
* Date Published
* Bookstore Product Code
* Author
* Content
* Relationships
** Place
** Series
** Person - the author of this document.
** Dataset
** Organization

<-- detail page breakdown -->

### Widget
Typically multiple entries in a list. Configurable attributes.
#### Fields
* Title
** Used internally by dotCMS to name this particular widget.
* Headline
** The title that will be displayed above the widget content on the website.
* Headline Type
** The size, style of the heading.
* Number of results
** Limit the total number of entries to display.
* Pagination
** Display the total number of results over a set number of pages, depending on the total entries available.
* Items per page
** If pagination is checked, how many items to show per page.

<-- widget breakdown -->

#### Developers
##### Content Type Detail Page Template
* //anr.msu.edu/shared/common_template/article-detail.vtl
##### Widget Template
* //anr.msu.edu/shared/vtl/widgets/common_template/article-listing.vtl

==================================================================================================


## Publication Issue

### Fields
* Host/folder
** This specifies which site the publication belongs to.
* Title
** The title of the publication. This will also auto-create the url title.
* URL Title
** Derived from the title field.
* Summary
** A short summary of the publication. Displayed in widget listings and on the publication page below the title.
* Image
** The main publication image which will be automatically cropped and resized for widget listings as well as publication detail pages.
* Image Description
** The alternative text for screen readers. Accessibility requirement.
* Social Media Image
** This image will be automatically used for social media sharing. According to current [[Open_graph#Our_Format_as_of_March_2017|social meta]], should be at least 1200px width x 600px height.
* Issue Number
* Date Published
* Publication PDF File
* Relationships
** Series - the publication is part of a series of these other publications.
** Organization - the organization this publicaiton belongs to.
** Article - articles related / belonging to this publication.

<-- detail page breakdown -->

### Widget
Typically multiple entries in a list. Configurable attributes.
#### Fields
* Title
** Used internally by dotCMS to name this particular widget.
* Headline
** The title that will be displayed above the widget content on the website.
* Headline Type
** The size, style of the heading.
* Number of results
** Limit the total number of entries to display.
* Pagination
** Display the total number of results over a set number of pages, depending on the total entries available.
* Items per page
** If pagination is checked, how many items to show per page.

<-- widget breakdown -->

#### Developers
##### Content Type Detail Page Template
* //anr.msu.edu/shared/common_template/article-detail.vtl
##### Widget Template
* //anr.msu.edu/shared/vtl/widgets/common_template/article-listing.vtl

==================================================================================================

## Job

==================================================================================================

## DataSet

### Fields
* Host/folder
** This specifies which site the dataset belongs to.
* Title
** The title of the dataset. This will also auto-create the url title.
* URL Title
** Derived from the title field.
* Summary
** A short summary of the dataset. Displayed in widget listings and on the publication page below the title.
* Image
** The main publication image which will be automatically cropped and resized for widget listings as well as publication detail pages.
* Image Description
** The alternative text for screen readers. Accessibility requirement.
* Social Media Image
** This image will be automatically used for social media sharing. According to current [[Open_graph#Our_Format_as_of_March_2017|social meta]], should be at least 1200px width x 600px height.
* Banner Image
** TODO
* Telephone
* Template
* Relationships
** Place - relate places to this dataset.
** Dataset - relate other datasets as a child to this dataset (related belongs to this dataset).
** Organization - relate organizations to this dataset.
** Digital Document - relate this dataset so it belongs to a single or set of digital document(s).
** Series - relate this dataset to a series or set of series.
** Course - relate this dataset to a course or multiple courses.
** Event - relate this dataset to an event or multiple events.
** Gallery Image - relate this dataset to a gallery image or multiple gallery images.
** Jobs - relate this dataset to a job or number of jobs.
** Gallery Video - relate this dataset to a gallery video or number of videos.
** Dataset - relate this dataset to a parent dataset(s).
** Person - relate this dataset to a parent person(s).
** Article - relate this dataset to an article or number of articles.

<-- detail page breakdown -->

### Widget
Typically multiple entries in a list. Configurable attributes.
#### Fields
* Title
** Used internally by dotCMS to name this particular widget.
* Headline
** The title that will be displayed above the widget content on the website.
* Headline Type
** The size, style of the heading.
* Number of results
** Limit the total number of entries to display.
* Pagination
** Display the total number of results over a set number of pages, depending on the total entries available.
* Items per page
** If pagination is checked, how many items to show per page.

<-- widget breakdown -->

#### Developers
##### Content Type Detail Page Template
* //anr.msu.edu/shared/common_template/article-detail.vtl
##### Widget Template
* //anr.msu.edu/shared/vtl/widgets/common_template/article-listing.vtl


==================================================================================================

## Place

### Fields
* Host/folder
** This specifies which site the place belongs to.
* Title
** The title of the place. This will also auto-create the url title.
* URL Title
** Derived from the title field.

<-- detail page breakdown -->

### Widget
Typically multiple entries in a list. Configurable attributes.
#### Fields
* Title
** Used internally by dotCMS to name this particular widget.
* Headline
** The title that will be displayed above the widget content on the website.
* Headline Type
** The size, style of the heading.
* Number of results
** Limit the total number of entries to display.
* Pagination
** Display the total number of results over a set number of pages, depending on the total entries available.
* Items per page
** If pagination is checked, how many items to show per page.

<-- widget breakdown -->

#### Developers
##### Content Type Detail Page Template
* //anr.msu.edu/shared/common_template/article-detail.vtl
##### Widget Template
* //anr.msu.edu/shared/vtl/widgets/common_template/article-listing.vtl

==================================================================================================

## Organization

### Fields
* Host/folder
** This specifies which site the organization belongs to.
* Title
** The title of the organization. This will also auto-create the url title.
* URL Title
** Derived from the title field.

<-- detail page breakdown -->

### Widget
Typically multiple entries in a list. Configurable attributes.
#### Fields
* Title
** Used internally by dotCMS to name this particular widget.
* Headline
** The title that will be displayed above the widget content on the website.
* Headline Type
** The size, style of the heading.
* Number of results
** Limit the total number of entries to display.
* Pagination
** Display the total number of results over a set number of pages, depending on the total entries available.
* Items per page
** If pagination is checked, how many items to show per page.

<-- widget breakdown -->

#### Developers
##### Content Type Detail Page Template
* //anr.msu.edu/shared/common_template/article-detail.vtl
##### Widget Template
* //anr.msu.edu/shared/vtl/widgets/common_template/article-listing.vtl

==================================================================================================

## Series

### Fields
* Host/folder
** This specifies which site the series belongs to.
* Title
** The title of the series. This will also auto-create the url title.
* URL Title
** Derived from the title field.

<-- detail page breakdown -->

### Widget
Typically multiple entries in a list. Configurable attributes.
#### Fields
* Title
** Used internally by dotCMS to name this particular widget.
* Headline
** The title that will be displayed above the widget content on the website.
* Headline Type
** The size, style of the heading.
* Number of results
** Limit the total number of entries to display.
* Pagination
** Display the total number of results over a set number of pages, depending on the total entries available.
* Items per page
** If pagination is checked, how many items to show per page.

<-- widget breakdown -->

#### Developers
##### Content Type Detail Page Template
* //anr.msu.edu/shared/common_template/article-detail.vtl
##### Widget Template
* //anr.msu.edu/shared/vtl/widgets/common_template/article-listing.vtl

==================================================================================================

## Page Asset

### Fields
* Host/folder
** This specifies which site the page asset belongs to.
* Title
** The title of the page asset. This will also auto-create the url title.
* URL Title
** Derived from the title field.

<-- detail page breakdown -->

### Widget
Typically multiple entries in a list. Configurable attributes.
#### Fields
* Title
** Used internally by dotCMS to name this particular widget.
* Headline
** The title that will be displayed above the widget content on the website.
* Headline Type
** The size, style of the heading.
* Number of results
** Limit the total number of entries to display.
* Pagination
** Display the total number of results over a set number of pages, depending on the total entries available.
* Items per page
** If pagination is checked, how many items to show per page.

<-- widget breakdown -->

#### Developers
##### Content Type Detail Page Template
* //anr.msu.edu/shared/common_template/article-detail.vtl
##### Widget Template
* //anr.msu.edu/shared/vtl/widgets/common_template/article-listing.vtl

==================================================================================================
